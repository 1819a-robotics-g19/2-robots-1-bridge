#include <Servo.h> 
#include <Wire.h>
#include <L3G.h>
#include <LSM303.h>

LSM303 compass;
Servo myservo;

// Globaalsed muutujad
int kiirus = 1600;
int moodud[100];
int pos = 0;
bool tostan = false;

void setup() {
  Wire.begin();
  myservo.attach(9);
  Serial.begin(9600);

  // Sild maas lüliti
  pinMode(3,INPUT);
  digitalWrite(3,HIGH); //enable pullup resistor

  // Kompassi seadistamine
  compass.init();
  compass.enableDefault();
  compass.m_min = (LSM303::vector<int16_t>){-32767, -32767, -32767};
  compass.m_max = (LSM303::vector<int16_t>){+32767, +32767, +32767};
}

void loop() {

  // Kui tuleb info sisse, et silda tuleb tõsta või langetada
  if (Serial.available() > 0) {
    int muutuja = Serial.parseInt();
    
    // Tõsta silda
    if (muutuja == 10) {
      kiirus = 1400;
      tostan = true;
    }

    // Langeta silda
    if (muutuja == 20) {
      kiirus = 1600;
      tostan = false;
    }
  }
  
  myservo.writeMicroseconds(kiirus);

  // Kompassi väärtuse lugemine
  compass.read();
  int vaartus = compass.a.z;

  // Kaalutud keskmise leidmine
  moodud[pos] = vaartus;
  pos = (pos+1)%100;
  long summa = 0;
  for (int el : moodud) {
    summa += el;
  }
  summa = summa/100;

  // Kontrollib ainult langetusel seda
  if (digitalRead(3) == LOW and tostan == false) {
    kiirus = 1500;
    Serial.println("Sild all");    
  }

  // Kontrollib ainult tõstmisel seda
  if (summa > -7000 and tostan == true) {
    kiirus = 1500;
    Serial.println("Sild üleval"); 
  }
}
