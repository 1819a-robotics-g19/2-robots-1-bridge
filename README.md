﻿# 2 robots 1 bridge

The project will be Cross the river with 1 bridge and 2 robots - 1 under, 1 over. The robots will need to communicate the order in which they cross the bridge.

### Pair A - Alar and Timo

#### Overview

Pair A is going to build a bridge. We think drawbridge concept should be the best for us. We have some ideas how to make the bridge to work, like do we have 1 or 2 ropes etc. For that we are
going to a make bridge prototype at the latest end of week 46. Also pair B can use the prototype for manufacturing their own code. In addition to making the wooden/plastic frame for bridge 
we have to write code for bridge lowering and rising. As much as we have talked with Ingmar and Taavi, we preffer using WiFi connection for robot and bridge communication. The communication
should be pretty simple, like "lower" and "rise". If we make a good progress with our project we could use more specific communication for asking bridge status. We put a lot of effort for
making the bridge construction. Electrical side and programming should take about 25% of the work. Right now we are thinking, that maybe we can use only WiFi module for controlling the servo.
It means that we don't have use one more arduino or raspberry for making the bridge work. We have some questions we have to find out. Like, does the bridge have to be wireless or it can be powered with
cord. Our work will be divided like 25% programming and electrical side, 40% bridge construction and 35% testing with pair B and making posters etc. If we are down in our plan, then we have 
some reserve time in Week 49 and 50 when to catch up. 

Here is our proof of consept video: https://streamable.com/mtq4z . We pasted the same video to TU datadrop. In so far we have working bridge prototype. The bridge is in prototype stage, because
we have to make it a little bit bigger. Also for the finished product we are going to place colored paper to mark the bridge frame for robots and bridge floor, where robots can drive across. Right 
now we used one servo, but we think we need two servos, just for safety. As we think we should be in our time schedule. 

Another video taken at 7th December: https://streamable.com/hiuve . Here we have working bridge rising and lowering system. We use multisensor and simple wire button for determining, in which state 
the bridge is. We have solved the problems like bridge size, counterweight and bridge own weight. As we see now, we can use one servo, which is very good news for us. Next task is programming 
WiFi module to send lowering/rising commands over WiFi.

#### Challenges and Solutions

* Bridge weight - make the bridge as light as possible and using two servos. Also using better transmission for servos. 
* Bridge size - we have to exactly measure the robot size and make as small bridge as we can for smaller weight. But we have to leave enouch space for robot driving mistakes and markings.
* Voltage and current for servos - powering two servos from arduino or ESP8266 isn't very good idea, so we have to find another power source, like Li-Po etc.
* Bridge counterweight - the bridge moving part is so heavy compared to standing parts. To solve falling down problem we have to weld some metal counterweights for that.

|Time| Alar and Timo    | Ingmar and Taavi       | 
|-----------------|---------------------------|---------------------------| 
|Week 45 (5-11.11)| Decide the specifics of the bridge ||
|Week 46 (12-18.11)| Prototype of drawbridge(bridge frame)   | TBD      |   
|Week 47 (19-25.11)| Proof of concept (bridge frame) | TBD      | 
|Week 48 (26-2.12)| Electonic and programming | TBD      |       
|Week 49 (3-9.12)| Testing-testing, fixing bugs and improving bridge (with pair B)| TBD     |
|Week 50 (10-16.12)| Making poster and getting ready for live demo (with pair B) | TBD      |        


| Component     | How many      | 
| ------------- |---------------| 
| ESP8266       | 1 | 
| USB camera      | 2      |   
| GoPiGo | 2      |
| Continuous servo | 1      |
| Raspberry pi | 2      |
| Ultrasonic distance sensor| 2 |
| Building material| unk |



## Team Members: 

 - Alar Tiideberg
 - Timo Tiirats
 - Taavi Eomäe
 - Ingmar Mõtsmees
