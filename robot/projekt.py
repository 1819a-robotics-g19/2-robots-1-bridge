# -*- coding: utf-8 -*-

import numpy as np
import cv2
import time
import os.path
from matplotlib import pyplot as plt
import gopigo as go

# global variable for determining gopigo speed
gospeed = 100
go_left_speed = 100
go_right_speed = 100
# global variable for video feed
cap = None



	

def init():
	global cap, gospeed ,lB, lG, lR, hB, hG, hR
	
	
	
	# This function should do everything required to initialize the robot.
	# Among other things it should open the camera and set gopigo speed.
	# Some of this has already been filled in.
	# You are welcome to add your own code, if needed.

	cap = cv2.VideoCapture(0)
	go.set_speed(gospeed)
	valge = [[],[]]



	lB = 40
	lG = 0
	lR = 35
	hB = 255
	hG = 255
	hR = 200



	return


# TASK 1
def get_line_location(frame,mask):
    # This function should use frame from camera to determine line location.
    # It should return location of the line in the frame.
    # Feel free to define and use any global variables you may need.
    # YOUR CODE HERE
	r = [len(frame)-10, len(frame), 0, len(frame[0])]
	frame=frame[r[0]:r[1], r[2]:r[3]]
	

	#You will need this later
	#frame = cv2.cvtColor(frame, ENTER_CORRECT_CONSTANT_HERE)
	frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	# colour detection limits

	lowerLimits = np.array(mask[0])
	upperLimits = np.array(mask[1])

	# Our operations on the frame come here
	thresholded = cv2.inRange(frame, lowerLimits, upperLimits)

	
	kernel = np.ones((5,5),np.uint8)
	dilation = cv2.dilate(thresholded,kernel,iterations = 1)
	erosion = cv2.erode(dilation,kernel,iterations = 1)
	
	
	outimage = cv2.bitwise_and(frame, frame, mask = thresholded)

		
	
	cv2.imshow('thresholded', frame)
	#print(np.sum(np.nonzero(thresholded[1]))
	
	return np.mean(np.nonzero(thresholded[1])),np.sum(np.nonzero(thresholded[1]))





def pid_controller(linelocation, viga2,vigadesumma):
    # This function should use the line location to implement PID controller.
    # Feel free to define and use any global variables you may need.
    # YOUR CODE HERE
	
	if np.isnan(linelocation):

		print("null")
		return 0
	
	elif viga2 == 0:
		
		viga = 320 - int(linelocation)
		return  viga
	
	else:
		
		viga = 320 - int(linelocation)
		x = 0.1
		tuletis = viga - viga2
		valem = 0.18 * viga + vigadesumma * 0.003 + 2.5 * tuletis 
		print("viga " + str(viga) + " summa " + str(vigadesumma) + " viga2 " + str(tuletis))
		#valem = 0.06 * viga + vigadesumma * 0.02824+ 0.03188 * viga2 
		#vigadesumma = integral
		#tuletis = tuletis
		


		
		
	
    
	return viga


# Initialization
init()
viga2 = 0
vigadesumma = 0
state = 0
while True:
    # We read information from camera.
	ret, frame = cap.read()
    #cv2.imshow('Original', frame)
	valge = [[0,0,230],[255,255,255]]
	roheline = [[0,75,0],[84,112,255]]
	punane = [[0,175,0],[255,255,184]]
	sinine = [[105,183,0],[255,255,255]]
	treshholdid = [[[105,183,0],[255,255,255]],[[0,0,230],[255,255,255]],[[0,75,0],[84,112,255]],[[0,248,0],[209,255,203]]]

    
	linelocation_praegune = get_line_location(frame,treshholdid[state % 3])
	linelocation_next = get_line_location(frame,treshholdid[(state + 1) % 3])

	
	if np.isnan(linelocation_praegune[0])	== False and linelocation_praegune[1] > 11000 :
		x = "lul"
			
	elif np.isnan(linelocation_next[0])	== False and linelocation_next[1] > 11000 :

		print("j�rgmine ja state =" + str(state))
		state += 1


		

	# if not np.isnan(linelocation):
		
		# vigadesumma += 320 - int(linelocation)
		
	# else:
		# vigadesumma = 0

    # Task 5: uncomment the following line and implement pid_controller function.
	#viga2 = pid_controller(linelocation, viga2, vigadesumma)
	

    
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break
		
with open("tresh.txt" , "w") as f:
	f.write(str(lB) + "\n")
	f.write(str(lG) + "\n")
	f.write(str(lR) + "\n")
	f.write(str(hB) + "\n")
	f.write(str(hG) + "\n")
	f.write(str(hR) + "\n")
cap.release()
cv2.destroyAllWindows()
go.stop()
